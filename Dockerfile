FROM openjdk:16-slim-buster

# Antlr
ENV ANTLR_VERSION 4.13.1
ENV CLASSPATH .:/usr/local/lib/antlr-complete.jar:$CLASSPATH

COPY ./antlr.sh /usr/bin/antlr
RUN chmod +x /usr/bin/antlr && ln -s /usr/bin/antlr /usr/bin/antlr4

ADD https://www.antlr.org/download/antlr-${ANTLR_VERSION}-complete.jar /usr/local/lib/antlr-complete.jar
RUN chmod +r /usr/local/lib/antlr-complete.jar

# Node
ENV NODE_VERSION=18.20.2
ENV NVM_DIR /usr/local/nvm
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH
RUN apt-get update \
  && apt-get install -y curl --no-install-recommends \
  && apt-get -y autoclean \
  && mkdir $NVM_DIR \
  && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash \
  && . ${NVM_DIR}/nvm.sh \
  && nvm install ${NODE_VERSION} \
  && nvm use ${NODE_VERSION} \
  && node -v \
  && npm -v