Changelog
=========

Version 1.3.0
-------------

- [#5](https://gitlab.com/maniascript/antlr-docker/-/issues/5) Update node to 18.20.2 (LTS) and antlr to 4.13.1

Version 1.2.1
-------------

- [#4](https://gitlab.com/maniascript/antlr-docker/-/issues/4) Fix node integration

Version 1.2.0
-------------

- [#4](https://gitlab.com/maniascript/antlr-docker/-/issues/4) Add node to the image